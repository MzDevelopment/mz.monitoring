document.addEventListener('DOMContentLoaded', async function() {
    M.Sidenav.init(document.querySelectorAll('.sidenav'));

    // get existing html-elements
    var content = document.getElementById('content');
    var loadingAnimation = document.getElementById('loading-animation');

    // remove loading-assets
    loadingAnimation.parentNode.removeChild(loadingAnimation);
    content.removeChild(document.getElementById('main-content'));
    
    // append template
    var contentWrapper = document.getElementById('content-wrapper-template').content.querySelector('main');
    content.appendChild(contentWrapper);
    var contentContainer = document.getElementById('content-container');
    var contentItem = document.getElementById('item-template').content.querySelector('li');
    var clients = await queryClients();
    clients.forEach((item) => {
        var contentItemEntity = document.importNode(contentItem, true);
        contentContainer.append(contentItemEntity);
    });
});

async function queryClients() {
    var response = await fetch('/data/clients');
    return await response.json();
}
