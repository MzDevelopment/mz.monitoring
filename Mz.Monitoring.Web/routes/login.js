var express = require('express');
var router = express.Router();
var { MongoClient } = require('mongodb');

var databaseConnectionString = 'mongodb+srv://serviceAccount:dJnzjv16JlO09_X7W@data.cszyd.mongodb.net?retryWrites=true&w=majority';
var databaseClient = new MongoClient(databaseConnectionString);

/* GET home page. */
router.get('/', (_req, res) => {
    res.render('login');
});

router.post('/', async (req, res) => {
    await databaseClient.connect();
    var monitoringDatabase = databaseClient.db('MonitoringData');
    var userCollection = monitoringDatabase.collection('user');
    var userAccount = await userCollection.findOne({ username: req.body.username });
    if (userAccount.username == req.body.username && userAccount.password == req.body.password) {
        session = req.session;
        session.userId = req.body.username;
        res.redirect('/clients');
    }
    else {
        res.render('login');
    }
});

module.exports = router;
