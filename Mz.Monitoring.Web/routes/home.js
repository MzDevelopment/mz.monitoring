var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (_req, res) => {
    res.render('home');
});

module.exports = router;
