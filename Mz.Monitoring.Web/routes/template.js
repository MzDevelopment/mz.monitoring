var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res) => {
	session = req.session;
	if (session.userId) {
		res.render('template');
	}
	else {
		res.render('login');
	}
});

module.exports = router;
