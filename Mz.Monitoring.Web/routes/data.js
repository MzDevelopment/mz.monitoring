var express = require('express');
var router = express.Router();
var { MongoClient } = require('mongodb');

var databaseConnectionString = 'mongodb+srv://serviceAccount:dJnzjv16JlO09_X7W@data.cszyd.mongodb.net?retryWrites=true&w=majority';
var databaseClient = new MongoClient(databaseConnectionString);

/* GET home page. */
router.get('/clients', async (req, res) => {
    session = req.session;
	if (session.userId) {
        await databaseClient.connect();
        var monitoringDatabase = databaseClient.db('MonitoringData');
        var clientCollection = monitoringDatabase.collection('client');
		var clientCursor = clientCollection.find();
		var clientDataCollection = new Array(await clientCursor.count());
		var i = 0;
        await clientCursor.forEach((item) => {
			clientDataCollection[i] = { "name": item.name, "os": item.os };
			i++;
		});
		res.send(clientDataCollection);
	}
	else {
		res.redirect('/login');
	}
});

module.exports = router;
