﻿namespace Mz.Monitoring.SharedComponents.Enums
{
    public enum MessageTypes
    {
        Default = 0,
        Authentication = 1,
        CpuData = 2,
        TaskInitiation = 3
    }
}
