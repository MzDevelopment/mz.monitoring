﻿using System.IO;
using System.Globalization;

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Mz.Monitoring.SharedComponents.Interfaces.Data;

namespace Mz.Monitoring.SharedComponents.Models.Data
{
    public class CpuData : IData
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        private ObjectId Id { get; set; }
        private float CpuUsagePercent { get; set; }
        public ObjectId ClientId { get; set; }

        public CpuData(ObjectId relatedClientId, float cpuUsagePercent)
        {
            ClientId = relatedClientId;
            Id = ObjectId.GenerateNewId();
            CpuUsagePercent = cpuUsagePercent;
        }

        public byte[] Serialize()
        {
            using var memoryStream = new MemoryStream();
            using (var binaryWriter = new BinaryWriter(memoryStream))
            {
                binaryWriter.Write(Id.ToString());
                binaryWriter.Write(
                    CpuUsagePercent.ToString(CultureInfo.InvariantCulture.NumberFormat));
            }
            return memoryStream.ToArray();
        }

        public CpuData Deserialize(byte[] data)
        {
            using var memoryStream = new MemoryStream(data);
            using var binaryReader = new BinaryReader(memoryStream);
            Id = ObjectId.Parse(binaryReader.ReadString());
            CpuUsagePercent = float.Parse(binaryReader.ReadString(),
                CultureInfo.InvariantCulture.NumberFormat);
            return this;
        }

    }
}
