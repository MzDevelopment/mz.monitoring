﻿using MongoDB.Bson;

namespace Mz.Monitoring.SharedComponents.Interfaces.Data
{
    public interface IData
    {
        ObjectId ClientId { get; set; }
    }
}
