﻿using System;
using System.Text;
using System.Collections.Generic;

using Serilog;
using WatsonTcp;
using MongoDB.Bson;

using Mz.Monitoring.SharedComponents.Enums;
using Mz.Monitoring.SharedComponents.Models.Data;

// ReSharper disable MemberCanBeMadeStatic.Local

namespace Mz.Monitoring.Server.Manager
{
    internal class ServerManager
    {
        public readonly WatsonTcpServer DataServer;
        private readonly DatabaseManager _databaseManager;

        public ServerManager(string listeningAddress, int listeningPort,
            DatabaseManager databaseManager)
        {
            _databaseManager = databaseManager;
            DataServer = new WatsonTcpServer(listeningAddress, listeningPort);
        }

        public void SetupDataServer()
        {
            DataServer.Events.ClientConnected += Events_ClientConnected;
            DataServer.Events.MessageReceived += Events_MessageReceived;
            DataServer.Events.ServerStarted += Events_ServerStarted;
            DataServer.Events.ClientDisconnected += Events_ClientDisconnected;
            DataServer.Start();
        }

        private void Events_ServerStarted(object sender, EventArgs e)
        {
            Log.Logger.Information("Server was successfully started");
        }

        private async void Events_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            var messageType = int.Parse(e.Metadata["messageType"].ToString() ?? "0");
            switch (messageType)
            {
                case (int)MessageTypes.CpuData:
                    var deserializedCpuData = new CpuData(ObjectId.Empty, 0).Deserialize(e.Data);
                    Log.Logger.Information($"CPU-Data has been received from [{e.IpPort}]");
                    await _databaseManager.InsertCpuDataAsync(deserializedCpuData);
                    break;
                default:
                    Log.Logger.Warning(
                        $"Unknown messageType [{e.Metadata["messageType"]}] has been received from [{e.IpPort}]");
                    break;
            }
        }

        private void Events_ClientConnected(object sender, ConnectionEventArgs e)
        {
            Log.Logger.Information($"Client connected from [{e.IpPort}]");
            try
            {
                var synchronizationResponse = DataServer.SendAndWait(5000, e.IpPort, null,
                    new Dictionary<object, object> {{"messageType", MessageTypes.Authentication}});
                var authenticationInformation =
                    Encoding.UTF8.GetString(synchronizationResponse.Data).Split(':');
                //TODO: Query Database for Credential
                Log.Logger.Information(
                    $"Client authenticated with Key [{authenticationInformation[0]}]");
                DataServer.Send(e.IpPort, null,
                    new Dictionary<object, object> {{"messageType", MessageTypes.TaskInitiation}});
            }
            catch (TimeoutException timeoutException)
            {
                //TODO: Actually send termination Request
                Log.Logger.Error(timeoutException,
                    $"No ClientId was provided by [{e.IpPort}]! Sending termination Request");
            }
        }

        private void Events_ClientDisconnected(object sender, DisconnectionEventArgs e)
        {
            Log.Logger.Information($"Client disconnected from [{e.IpPort}]");
        }
    }
}
