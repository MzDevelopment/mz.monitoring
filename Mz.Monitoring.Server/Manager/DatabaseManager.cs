﻿using System;
using System.Linq;
using System.Threading.Tasks;

using Serilog;
using MongoDB.Driver;
using Mz.Monitoring.SharedComponents.Models.Data;

namespace Mz.Monitoring.Server.Manager
{
    internal class DatabaseManager
    {
        private readonly string[] _databaseCollections = {
            "cpuData",
            "authenticationInformation"
        };

        private const string DatabaseName = "MonitoringData";

        private readonly MongoClient _databaseClient;

        public DatabaseManager(string databaseConnectionString)
        {
            _databaseClient = new MongoClient(databaseConnectionString);
        }

        public async Task SetupDatabaseAsync()
        {
            Log.Logger.Information("Setting up the Database");
            var database = _databaseClient.GetDatabase(DatabaseName);
            foreach (var databaseCollection in _databaseCollections)
            {
                try
                {
                    await database.CreateCollectionAsync(databaseCollection);
                    Log.Logger.Information(
                        $"Collection [{databaseCollection}] was created successfully");
                }
                catch (MongoCommandException databaseConfigurationException)
                {
                    Log.Logger.Information(databaseConfigurationException,
                        $"Collection [{databaseCollection}] seems to already exist");
                }
            }
        }

        public async Task InsertCpuDataAsync(CpuData cpuData)
        {
            var database = _databaseClient.GetDatabase(DatabaseName);
            try
            {
                var databaseCollection =
                    database.GetCollection<CpuData>(
                        _databaseCollections.FirstOrDefault(x => x.Contains("cpu")));
                await databaseCollection.InsertOneAsync(cpuData);
                Log.Logger.Information(
                    $"CPU-Data has been successfully written to DB! ObjectId: [{cpuData.ClientId}]");
            }
            catch (Exception genericException)
            {
                Log.Logger.Warning(genericException,
                    $"An Error occurred during CPU-Data insertion! ObjectId: [{cpuData.ClientId}]");
            }
        }
    }
}
