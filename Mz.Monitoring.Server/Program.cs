﻿using System;
using System.Threading.Tasks;

using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

using Mz.Monitoring.Server.Manager;

namespace Mz.Monitoring.Server
{
    internal class Program
    {
        private static async Task Main()
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console(theme: AnsiConsoleTheme.Code,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {NewLine}")
                .CreateLogger();
            var databaseManager = new DatabaseManager(
                "mongodb+srv://serviceAccount:dJnzjv16JlO09_X7W@data.cszyd.mongodb.net?retryWrites=true&w=majority");
            await databaseManager.SetupDatabaseAsync();
            var serverManager = new ServerManager("127.0.0.1", 24000, databaseManager);
            serverManager.SetupDataServer();
            await Task.Delay(TimeSpan.FromMilliseconds(500));
            while (serverManager.DataServer.IsListening)
            {
                Log.Logger.Information("Server is running normally");
                await Task.Delay(TimeSpan.FromMinutes(15));
            }
        }
    }
}
