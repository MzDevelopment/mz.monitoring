﻿using Xunit;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

// ReSharper disable StringLiteralTypo
// ReSharper disable RedundantAssignment
// ReSharper disable NotAccessedVariable

namespace Mz.Monitoring.Tests
{
    public class CpuTests
    {
        [Fact]
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Interoperability", "CA1416:Plattformkompatibilität überprüfen", Justification = "<Ausstehend>")]
        public async Task GetCpuUsage()
        {
            var cpuPerformanceCounter =
                new PerformanceCounter("Processor", "% Processor Time", "_Total");
            var cpuPercentageUsage = cpuPerformanceCounter.NextValue();
            await Task.Delay(TimeSpan.FromMilliseconds(250));
            cpuPercentageUsage = cpuPerformanceCounter.NextValue();
        }
    }
}
