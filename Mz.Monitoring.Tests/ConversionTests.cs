﻿using System.Globalization;
using Xunit;

namespace Mz.Monitoring.Tests
{
    public class ConversionTests
    {
        [Fact]
        public void ConvertFloat()
        {
            var floatString = 15.5f.ToString(CultureInfo.InvariantCulture.NumberFormat);
            var parsedFloat = float.Parse(floatString, CultureInfo.InvariantCulture.NumberFormat);
        }
    }
}
