﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using AdysTech.CredentialManager;
using Xunit;

namespace Mz.Monitoring.Tests
{
    public class CredentialTests
    {
        [Fact]
        public ICredential CheckForCredential()
        {
            var credential = CredentialManager.GetICredential("Mz.Monitoring");
            if (credential != null) return credential;
            credential = new NetworkCredential("username", "password").ToICredential();
            credential.TargetName = "Mz.Monitoring";
            credential.Attributes = new Dictionary<string, object>
            {
                {"debugAttributeKey", "debugAttributeValue"}
            };
            credential.SaveCredential();
            return credential;
        }
    }
}
