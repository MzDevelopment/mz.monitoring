﻿using System.Threading;
using System.Threading.Tasks;

using WatsonTcp;

namespace Mz.Monitoring.Windows.Interfaces
{
    internal interface IDataManager
    {
        Task QueryDataAsync(WatsonTcpClient dataClient, CancellationToken cancellationToken);
    }
}
