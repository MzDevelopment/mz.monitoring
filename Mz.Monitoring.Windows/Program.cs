﻿using System;
using System.Threading;
using System.Threading.Tasks;

using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

using Mz.Monitoring.Windows.Managers;

namespace Mz.Monitoring.Windows
{
    internal class Program
    {
        private static async Task Main()
        {
            Log.Logger = new LoggerConfiguration().WriteTo.Console(theme: AnsiConsoleTheme.Code,
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {NewLine}")
                .CreateLogger();
            var cancellationTokenSource = new CancellationTokenSource();
            var clientManager =
                new ClientManager("127.0.0.1", 24000, cancellationTokenSource.Token);
            clientManager.SetupDataClient();
            while (!cancellationTokenSource.IsCancellationRequested)
            {
                Log.Logger.Information("Application is running normally");
                await Task.Delay(TimeSpan.FromMinutes(15), cancellationTokenSource.Token);
            }
        }
    }
}
