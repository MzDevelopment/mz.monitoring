﻿using System;
using System.Net;
using System.Threading;

using Serilog;
using WatsonTcp;
using AdysTech.CredentialManager;

using Mz.Monitoring.SharedComponents.Enums;

// ReSharper disable MemberCanBeMadeStatic.Local

namespace Mz.Monitoring.Windows.Managers
{
    internal class ClientManager
    {
        private readonly WatsonTcpClient _dataClient;
        private readonly CancellationToken _cancellationToken;

        public ClientManager(string connectionAddress, int connectionPort,
            CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            _dataClient = new WatsonTcpClient(connectionAddress, connectionPort);
        }

        public void SetupDataClient()
        {
            _dataClient.Events.ServerConnected += Events_ServerConnected;
            _dataClient.Events.MessageReceived += Events_MessageReceived;
            _dataClient.Callbacks.SyncRequestReceived = SyncRequestReceived;
            _dataClient.Connect();
        }

        private SyncResponse SyncRequestReceived(SyncRequest synchronizationRequest)
        {
            var messageType =
                int.Parse(synchronizationRequest.Metadata["messageType"].ToString() ?? "0");
            switch (messageType)
            {
                case (int)MessageTypes.Authentication:
                    var credential = CredentialManager.GetCredentials("Mz.Monitoring");
                    if (credential != null)
                    {
                        return new SyncResponse(synchronizationRequest, credential.Password);
                    }
                    credential = new NetworkCredential(Guid.NewGuid().ToString(),
                        Guid.NewGuid().ToString());
                    CredentialManager.SaveCredentials("Mz.Monitoring", credential);
                    return new SyncResponse(synchronizationRequest,
                        credential.UserName + ':' + credential.Password);
                default:
                    Log.Logger.Warning(
                        $"Unknown messageType [{synchronizationRequest.Metadata["messageType"]}] has been received from [{synchronizationRequest.IpPort}]");
                    return new SyncResponse(synchronizationRequest, "PLACEHOLDER");
            }
        }

        private void Events_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            var messageType = int.Parse(e.Metadata["messageType"].ToString() ?? "0");
            switch (messageType)
            {
                case (int)MessageTypes.TaskInitiation:
                    new TaskManager(_cancellationToken, _dataClient).InitiateTaskStart();
                    break;
                default:
                    Log.Logger.Warning(
                        $"Unknown messageType [{e.Metadata["messageType"]}] has been received from [{e.IpPort}]");
                    break;
            }
        }

        private void Events_ServerConnected(object sender, ConnectionEventArgs e)
        {
            Log.Logger.Information("Client successfully connected to Server");
        }
    }
}
