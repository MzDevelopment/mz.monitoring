﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;

using Serilog;
using WatsonTcp;
using MongoDB.Bson;
using System.Diagnostics;

using Mz.Monitoring.Windows.Interfaces;
using Mz.Monitoring.SharedComponents.Enums;
using Mz.Monitoring.SharedComponents.Models.Data;

// ReSharper disable StringLiteralTypo
// ReSharper disable MemberCanBeMadeStatic.Local

namespace Mz.Monitoring.Windows.Managers.Data
{
    internal class CpuManager : IDataManager
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage(
            "Interoperability", "CA1416:Plattformkompatibilität überprüfen",
            Justification = "<Ausstehend>")]
        public async Task QueryDataAsync(WatsonTcpClient dataClient,
            CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                var cpuPerformanceCounter =
                    new PerformanceCounter("Processor", "% Processor Time", "_Total");
                // ReSharper disable once RedundantAssignment
                var cpuPercentageUsage = cpuPerformanceCounter.NextValue();
                await Task.Delay(TimeSpan.FromMilliseconds(250), cancellationToken);
                cpuPercentageUsage = cpuPerformanceCounter.NextValue();
                await SendDataAsync(new CpuData(ObjectId.Empty, cpuPercentageUsage), dataClient, cancellationToken);
                Log.Logger.Information("Cpu-Check has been executed!");
                await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);
            }
        }
        
        private async Task SendDataAsync(CpuData cpuData, WatsonTcpClient dataClient,
            CancellationToken cancellationToken)
        {
            await dataClient.SendAsync(cpuData.Serialize(),
                new Dictionary<object, object> {{"messageType", MessageTypes.CpuData}},
                token: cancellationToken);
        }
    }
}
