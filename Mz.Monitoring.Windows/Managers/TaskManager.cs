﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mz.Monitoring.Windows.Managers.Data;
using WatsonTcp;

namespace Mz.Monitoring.Windows.Managers
{
    internal class TaskManager
    {
        private List<Task> _taskCollection;
        private readonly WatsonTcpClient _dataClient;
        private readonly CancellationToken _cancellationToken;

        public TaskManager(CancellationToken cancellationToken,
            WatsonTcpClient dataClient)
        {
            _dataClient = dataClient;
            _cancellationToken = cancellationToken;
        }

        public void InitiateTaskStart()
        {
            _taskCollection = new List<Task>
            {
                new CpuManager().QueryDataAsync(_dataClient, _cancellationToken)
            };
        }
    }
}
